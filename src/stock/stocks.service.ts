import { Injectable } from '@nestjs/common';
import { CreateStockDto } from './dto/create-stock.dto';
import { UpdateStockDto } from './dto/update-stock.dto';
import { Stock } from './entities/Stock.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class StocksService {
  constructor(
    @InjectRepository(Stock)
    private StocksRepository: Repository<Stock>,
  ) {}
  create(createStockDto: CreateStockDto): Promise<Stock> {
    return this.StocksRepository.save(createStockDto);
  }
  findAll() {
    return this.StocksRepository.find();
  }

  findOne(id: number) {
    return this.StocksRepository.findOneBy({ id: id });
  }

  async update(id: number, updateStockDto: UpdateStockDto) {
    await this.StocksRepository.update(id, updateStockDto);
    const Stock = await this.StocksRepository.findOneBy({ id });
    return Stock;
  }

  async remove(id: number) {
    const deleteStock = await this.StocksRepository.findOneBy({ id });
    return this.StocksRepository.remove(deleteStock);
  }
}
