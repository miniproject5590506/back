import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Stock {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  name: string;
  @Column()
  min: number;
  @Column()
  balance: number;
  @Column()
  unit: string;
  @Column({
    default: 'Available',
  })
  status: 'Available' | 'Low' | 'Out of Stock';
}
