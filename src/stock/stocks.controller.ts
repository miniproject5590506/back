import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { StocksService } from './stocks.service';
import { CreateStockDto } from './dto/create-stock.dto';
import { UpdateStockDto } from './dto/update-stock.dto';

@Controller('Stocks')
export class StocksController {
  constructor(private readonly StockService: StocksService) {}
  // Create
  @Post()
  create(@Body() createStockDto: CreateStockDto) {
    return this.StockService.create(createStockDto);
  }

  // Read All
  @Get()
  findAll() {
    return this.StockService.findAll();
  }
  // Read One
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.StockService.findOne(+id);
  }
  // Partial Update
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateStockDto: UpdateStockDto) {
    return this.StockService.update(+id, updateStockDto);
  }
  // Delete
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.StockService.remove(+id);
  }
}
