import { IsNotEmpty } from 'class-validator';

export class CreateStockDto {
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  min: number;
  @IsNotEmpty()
  balance: number;
  @IsNotEmpty()
  unit: string;
  @IsNotEmpty()
  status: 'Available' | 'Low' | 'Out of Stock';
}
